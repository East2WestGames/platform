package com.mediav.ads;

import com.mediav.ads.activity.SampleBannerActivity;
import com.mediav.ads.activity.SampleFloatBannerActivity;
import com.mediav.ads.activity.SampleInterstitialActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout linearLayout = new LinearLayout(this);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		setContentView(linearLayout, lp);
		
		this.setTitle("Demo");

		LinearLayout.LayoutParams btnlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		btnlp.topMargin = 10;
		btnlp.gravity = Gravity.CENTER_HORIZONTAL;
		
		Button sampleBannerBtn = new Button(this);
		sampleBannerBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, SampleBannerActivity.class);
				startActivity(intent);
			}
		});
		sampleBannerBtn.setText("条幅广告");
		linearLayout.addView(sampleBannerBtn, btnlp);
		
		Button sampleInterstitialBtn = new Button(this);
		sampleInterstitialBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, SampleInterstitialActivity.class);
				startActivity(intent);
			}
		});
		sampleInterstitialBtn.setText("插屏广告");
		linearLayout.addView(sampleInterstitialBtn, btnlp);
		
		Button sampleFloatBannerBtn = new Button(this);
		sampleFloatBannerBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, SampleFloatBannerActivity.class);
				startActivity(intent);
			}
		});
		sampleFloatBannerBtn.setText("浮动条幅广告");
		linearLayout.addView(sampleFloatBannerBtn, btnlp);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
