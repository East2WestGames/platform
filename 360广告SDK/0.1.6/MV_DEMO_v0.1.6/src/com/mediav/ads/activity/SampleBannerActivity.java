package com.mediav.ads.activity;

import com.mediav.ads.R;
import com.mediav.ads.sdk.adcore.Mvad;
import com.mediav.ads.sdk.interfaces.IMvBannerAd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class SampleBannerActivity extends Activity implements View.OnClickListener{
	
	private RelativeLayout adContainer = null;
	private IMvBannerAd bannerad = null;
	
	private Button banner_open_btn = null;
	private Button banner_close_btn = null;
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.simple_banner_layout);
		setTitle("条幅");
		
		banner_open_btn = (Button) findViewById(R.id.banner_open_btn);
		banner_open_btn.setOnClickListener(this);
		banner_close_btn = (Button) findViewById(R.id.banner_close_btn);
		banner_close_btn.setOnClickListener(this);
		adContainer = (RelativeLayout) findViewById(R.id.banner_adcontainer);
		
		/**
		 * 条幅广告加载完成后立即在广告容器中显示
		 */
		final String adSpaceid = "网站上获取此ID";
		bannerad = Mvad.showBanner(adContainer, SampleBannerActivity.this, adSpaceid, true);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.banner_open_btn:
			bannerad.showAds(this);
			break;
			
		case R.id.banner_close_btn:
			bannerad.closeAds();
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Mvad.activityDestroy(this);
	}
}
