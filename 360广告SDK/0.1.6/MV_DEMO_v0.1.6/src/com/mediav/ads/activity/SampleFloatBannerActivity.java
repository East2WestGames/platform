package com.mediav.ads.activity;

import com.mediav.ads.R;
import com.mediav.ads.sdk.adcore.Mvad;
import com.mediav.ads.sdk.adcore.Mvad.FLOAT_BANNER_SIZE;
import com.mediav.ads.sdk.adcore.Mvad.FLOAT_LOCATION;
import com.mediav.ads.sdk.interfaces.IMvFloatbannerAd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SampleFloatBannerActivity extends Activity implements View.OnClickListener{
	
	private IMvFloatbannerAd floatbannerad = null;
	
	private Button banner_open_btn = null;
	private Button banner_close_btn = null;
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.simple_banner_layout);
		setTitle("浮动条幅");
		
		banner_open_btn = (Button) findViewById(R.id.banner_open_btn);
		banner_open_btn.setOnClickListener(this);
		banner_close_btn = (Button) findViewById(R.id.banner_close_btn);
		banner_close_btn.setOnClickListener(this);
		
		/**
		 * 条幅广告加载完成后立即在广告容器中显示
		 */
		final String adSpaceid = "网站上获取此ID";
		floatbannerad = Mvad.showFloatbannerAd(this, adSpaceid, true, FLOAT_BANNER_SIZE.SIZE_DEFAULT, FLOAT_LOCATION.BOTTOM);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.banner_open_btn:
			final String adSpaceid = "网站上获取此ID";
			Mvad.showFloatbannerAd(this, adSpaceid, true, FLOAT_BANNER_SIZE.SIZE_DEFAULT, FLOAT_LOCATION.BOTTOM);
			break;
			
		case R.id.banner_close_btn:
			floatbannerad.closeAds();
			break;

		default:
			break;
		}
	}
}
