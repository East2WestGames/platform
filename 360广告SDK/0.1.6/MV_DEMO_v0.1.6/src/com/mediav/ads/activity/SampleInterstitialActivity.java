package com.mediav.ads.activity;

import com.mediav.ads.R;
import com.mediav.ads.sdk.adcore.Mvad;
import com.mediav.ads.sdk.interfaces.IMvAdEventListener;
import com.mediav.ads.sdk.interfaces.IMvInterstitialAd;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SampleInterstitialActivity extends Activity implements View.OnClickListener{

	private IMvInterstitialAd interstitialAd = null;
	
	private Button interstitial_open_btn = null;
	private Button interstitial_close_btn = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.simple_interstitial_layout);
		setTitle("插屏");
		
		interstitial_open_btn = (Button) findViewById(R.id.interstitial_open_btn);
		interstitial_open_btn.setOnClickListener(this);
		interstitial_close_btn = (Button) findViewById(R.id.interstitial_close_btn);
		interstitial_close_btn.setOnClickListener(this);
		
		/**
		 * 插屏广告加载完成后立即显示
		 */
		final String adSpaceid = "网站上获取此ID";
		
		//插屏
		interstitialAd = Mvad.showInterstitial(this, adSpaceid, true);
		
		interstitialAd.setAdEventListener(new IMvAdEventListener() {
			
			@Override
			public void onAdviewIntoLandpage() {
				
			}
			
			@Override
			public void onAdviewGotAdSucceed() {
				
			}
			
			@Override
			public void onAdviewGotAdFail() {
				
			}
			
			@Override
			public void onAdviewDismissedLandpage() {
				
			}
			
			@Override
			public void onAdviewDestroyed() {
				
			}
			
			@Override
			public void onAdviewClosed() {
				
			}
			
			@Override
			public void onAdviewClicked() {
				
			}
		});
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.interstitial_open_btn:
			interstitialAd.showAds(this);
			break;
			
		case R.id.interstitial_close_btn:
			
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Mvad.activityDestroy(this);
	}
}
