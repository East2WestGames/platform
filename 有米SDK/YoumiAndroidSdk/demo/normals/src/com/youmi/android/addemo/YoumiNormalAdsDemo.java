package com.youmi.android.addemo;

import java.util.ArrayList;

import com.noodlecake.anothercasesolved.dafg;
import com.noodlecake.anothercasesolved.br.daig;
import com.noodlecake.anothercasesolved.br.damg;
import com.noodlecake.anothercasesolved.br.damgListener;
import com.noodlecake.anothercasesolved.st.dbqg;
import com.noodlecake.anothercasesolved.st.dbrg;
import com.noodlecake.anothercasesolved.video.dbug;
import com.noodlecake.anothercasesolved.video.listener.VideoApkDownloadListener;
import com.noodlecake.anothercasesolved.video.listener.dbtg;
import com.noodlecake.anothercasesolved.video.listener.dbvg;
import com.noodlecake.anothercasesolved.video.model.VideoInfoModel;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class YoumiNormalAdsDemo extends Activity {

	Context context;

	// 下载列表
	private ArrayList<VideoInfoModel> downloadList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ad);
		context = this;

		setVideoAd();
		setSpotAd();
		showDownloadList();

		showBanner();
	}

	// 演示怎么获取广告的介绍资料并添加到界面中，在请求成功之后启动线程。
	Thread thread = new Thread(new Runnable() {

		@Override
		public void run() {
			final VideoInfoModel model = dbug.getInstance(context).catchVideoInfo(false);
			if (model != null) {
				new Handler(Looper.getMainLooper()).post(new Runnable() {

					@Override
					public void run() {
						RelativeLayout infoLayout = (RelativeLayout) findViewById(R.id.info_layout);
						infoLayout.setVisibility(View.VISIBLE);
						ImageView mMainImageView = (ImageView) findViewById(R.id.info_mainimage);
						if (model.image != null) {
							Bitmap mainImage = BitmapFactory.decodeFile(model.image);
							if (mainImage != null) {
								mMainImageView.setImageBitmap(mainImage);
								mMainImageView.setVisibility(View.VISIBLE);
							}
						}
						ImageView mIconView = (ImageView) findViewById(R.id.info_icon);
						if (model.icon != null) {
							Bitmap iconImage = BitmapFactory.decodeFile(model.icon);
							if (iconImage != null) {
								mIconView.setImageBitmap(iconImage);
							}
						}
						TextView mTitleView = (TextView) findViewById(R.id.info_title);
						if (model.name != null) {
							mTitleView.setText(model.name);
						}
						TextView mTextView = (TextView) findViewById(R.id.info_adtext);
						if (model.adText != null) {
							mTextView.setText(model.adText);
						}
					}
				});
			}
		}
	});

	private void setVideoAd() {

		// 视频设置，提供视频的各种设置,如设置退出提示语，更改退出按钮，加载中的logo。
		dbug.getInstance(context).getVideoAdSetting().setInterruptsTips("是否要退出视频");

		dbug.getInstance(context).tavy(new dbvg() {

			@Override
			public void onRequestSucceed() {
				// 提前展示视频广告的详情。包括名字，icon，图片，介绍。取消下面thread即可生效。
				// thread.start();
				Log.d("videoPlay", "请求成功");
			}

			@Override
			public void onRequestFail(int errorCode) {
				// 关于错误码的解读：-1为网络连接失败，请检查网络。-2007为无广告，-3312为该设备一天的播放次数已完,其他错误码一般为设备问题。
				Log.d("videoPlay", "请求失败，错误码为:" + errorCode);
			}

		});

		Button videoBtn = (Button) findViewById(R.id.showVideo);
		videoBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dbug.getInstance(context).tbfy(context, videoListener);

			}
		});

	}

	dbtg videoListener = new dbtg() {

		// 视频播放失败
		@Override
		public void onVideoPlayFail() {
			Log.d("videoPlay", "failed");
		}

		// 视频播放完成
		@Override
		public void onVideoPlayComplete() {
			Log.d("videoPlay", "complete");
			Toast.makeText(context, "您获得了1个金币的奖励", Toast.LENGTH_SHORT).show();
		}

		// 视频播放中途退出
		@Override
		public void onVideoPlayInterrupt() {
			Log.d("videoPlay", "interrupt");
			Toast.makeText(context, "视频未播放完成，无法获取奖励", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onDownloadComplete(String id) {
			Log.e("videoPlay", "download complete: " + id);
		}

		@Override
		public void onNewApkDownloadStart() {
			Log.e("videoPlay", "开始下载");
			setListener();
		}

		@Override
		public void onVideoLoadComplete() {
			Log.e("videoPlay", "视频加载完成");
		}
	};

	private void showDownloadList() {
		if (downloadList == null) {
			downloadList = dbug.getInstance(context).getApkDownloadTaskList();
		}
		if (downloadList.size() == 0) {
			Log.e("videoPlay", "size is 0");
		}
		for (int i = 0; i < downloadList.size(); i++) {
			Log.e("videoPlay", "id: " + downloadList.get(i).id + "    name: " + downloadList.get(i).name);
		}
	}

	// 可选，对每个下载任务进行监听。
	private void setListener() {
		if (downloadList.size() > 0) {
			final VideoInfoModel model = downloadList.get(downloadList.size() - 1);
			if (model.getDownloadListener() == null) {
				model.setDownloadListener(new VideoApkDownloadListener() {

					@Override
					public void onApkDownloaidStart() {
						Log.e("videoPlay", "开始下载   " + model.name);
					}

					@Override
					public void onApkDownloadSuccess() {
						Log.e("videoPlay", "下载成功   " + model.name);
					}

					@Override
					public void onApkDownloadProgressUpdate(long contentLength, long completeLength, int percent,
							long speedBytesPerS) {
						Log.e("videoPlay", "下载的百分比：" + percent + "   " + model.name);
					}

					@Override
					public void onApkDownloadFail() {
						Log.e("videoPlay", "下载失败");
					}
				});
			}
		}
	}

	// 安装APK,例如：安装第N个APK
	private void installApk(int num) {
		if (downloadList != null && downloadList.size() > num) {
			String id = downloadList.get(num).id;
			dbug.getInstance(context).handleInstallApk(id);
		}
	}

	// 安装最近一个APK
	private void installApk() {
		if (downloadList != null && downloadList.size() > 0) {
			dbug.getInstance(context).handleInstallApk();
		}
	}

	// 删除指定apk
	private void deleteAPK(int num) {
		if (downloadList != null && downloadList.size() > num) {
			String id = downloadList.get(num).id;
			dbug.getInstance(context).deleteApk(id);
		}
	}

	private void setSpotAd() {
		// 插播接口调用

		// 加载插播资源
		dbrg.tahy(context).taky();
		// 插屏出现动画效果，0:ANIM_NONE为无动画，1:ANIM_SIMPLE为简单动画效果，2:ANIM_ADVANCE为高级动画效果
		dbrg.tahy(context).tazy(
				dbrg.ANIM_ADVANCE);
		// 设置插屏动画的横竖屏展示方式，如果设置了横屏，则在有广告资源的情况下会是优先使用横屏图。
		dbrg.tahy(context).tbby(
				dbrg.ORIENTATION_PORTRAIT);
		Button spotBtn = (Button) findViewById(R.id.showSpot);
		spotBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// 展示插播广告，可以不调用loadSpot独立使用
				dbrg.tahy(context).tbey(context,
						new dbqg() {
							@Override
							public void tapy() {
								Log.i("YoumiAdDemo", "展示成功");
							}

							@Override
							public void taoy() {
								Log.i("YoumiAdDemo", "展示失败");
							}

							@Override
							public void tary() {
								Log.i("YoumiAdDemo", "展示关闭");
							}

							@Override
							public void taqy() {
								Log.i("YoumiAdDemo", "插屏点击");
							}

						}); // //

			}
		});
	}

	private void showBanner() {

		// 实例化LayoutParams(重要)
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT);
		// 设置广告条的悬浮位置
		layoutParams.gravity = Gravity.BOTTOM | Gravity.RIGHT; // 这里示例为右下角
		// 实例化广告条
		damg adView = new damg(context, daig.FIT_SCREEN);
		// 调用Activity的addContentView函数

		// 监听广告条接口
		adView.tayy(new damgListener() {

			@Override
			public void taty(damg arg0) {
				Log.i("YoumiAdDemo", "广告条切换");
			}

			@Override
			public void tany(damg arg0) {
				Log.i("YoumiAdDemo", "请求广告成功");

			}

			@Override
			public void tamy(damg arg0) {
				Log.i("YoumiAdDemo", "请求广告失败");
			}
		});
		((Activity) context).addContentView(adView, layoutParams);
	}

	@Override
	public void onBackPressed() {
		// 如果有需要，可以点击后退关闭插播广告。
		if (!dbrg.tahy(context).tady()) {
			// 弹出退出窗口，可以使用自定义退屏弹出和回退动画,参照demo,若不使用动画，传入-1
			super.onBackPressed();
		}
	}

	@Override
	protected void onStop() {
		// 如果不调用此方法，则按home键的时候会出现图标无法显示的情况。
		dbrg.tahy(context).tasy();
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		dbug.getInstance(context).onDestroy();
		dbrg.tahy(context).taly();
		super.onDestroy();
	}

}
