package pl.idreams.pottery;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import cn.cmgame.billing.api.BillingResult;
import cn.cmgame.billing.api.GameInterface;
import cn.egame.terminal.paysdk.EgamePay;
import cn.egame.terminal.paysdk.EgamePayListener;
import cn.egame.terminal.sdk.log.EgameAgent;

import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.Utils.UnipayPayResultListener;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity {
	public static String Mobile_pay_id,Telecom_pay_id,Unicom_pay_id,save_message;
	public static MainActivity instance;    
	private Context context;

	public static int cost;
	public static String Log_Tag="Max";
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		instance=this;
		context =this;	
		//init mobile
		if(GameSdkApplication.netcheck==1||GameSdkApplication.netcheck==0)
		{
			GameInterface.initializeApp(this);
			Log.e(Log_Tag, "init mobile");
		}
		//init telecom
		if(GameSdkApplication.netcheck==3)
		{
			EgamePay.init(this);
			Log.e(Log_Tag, "init telecom");
		}
	}

	@Override
	protected void onPause() {
		Log.e(Log_Tag, "MainActivity onPause");
		// TODO Auto-generated method stub
		if(GameSdkApplication.netcheck==2)
			Utils.getInstances().onPause(context);
		if(GameSdkApplication.netcheck==3)
			EgameAgent.onResume(this);
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.e(Log_Tag, "MainActivity onResume");
		// TODO Auto-generated method stub
		if(GameSdkApplication.netcheck==2)
			Utils.getInstances().onResume(context);
		if(GameSdkApplication.netcheck==3)
			EgameAgent.onResume(this);
		super.onResume();

	}

	public static Object getInstance() 
	{
		Log.e(Log_Tag, "instance");
		return instance;
	}


	public void Buy(final String pid) {
		
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				buy(pid);
			}
		});
	}


	public void buy(String message) {
		save_message=message;
		switch (message) {
		case "10diamonds":
			Mobile_pay_id="001";
			Telecom_pay_id="TOOL5";
			Unicom_pay_id="005";
			break;
		case "60diamonds":
			Mobile_pay_id="002";
			Telecom_pay_id="TOOL6";
			Unicom_pay_id="006";
			break;
		case "130diamonds":
			Mobile_pay_id="003";
			Telecom_pay_id="TOOL7";
			Unicom_pay_id="007";
			break;
		case "210diamonds":
			Mobile_pay_id="004";
			Telecom_pay_id="TOOL8";
			Unicom_pay_id="008";
			break;
		}
		//if it is mobile or can't read the SIM card
		if(GameSdkApplication.netcheck==1||GameSdkApplication.netcheck==0)
		{
			Log.e(Log_Tag, "Buy_mobile");
			GameInterface.doBilling(context, true, true, Mobile_pay_id, null, new GameInterface.IPayCallback() {
				
				@Override
				public void onResult(int resultCode, String billingIndex, Object obj) {
					// TODO Auto-generated method stub
					String result = "";
					final String index = billingIndex;
					switch(resultCode)
					{
						case BillingResult.SUCCESS:
							result = "支付成功";
							UnityPlayer.UnitySendMessage("Main Camera", "Success",save_message);
							break;
						case BillingResult.FAILED:
							result = "支付失败";
							break;
						default:
							result = "支付取消";
							Log.e(Log_Tag, save_message);	
							break;
					}
					Toast.makeText(instance, result, Toast.LENGTH_LONG).show();
				}
			});
		}
		//if it is Unicon
		if(GameSdkApplication.netcheck==2)
		{
			Log.e(Log_Tag, "Buy_Unicon");
			Utils.getInstances().pay(MainActivity.this,Unicom_pay_id,new paylistener());
		}
		
		//if it is telecom
		if(GameSdkApplication.netcheck==3)
		{
			Log.e(Log_Tag, "Buy_telecom");
			HashMap<String, String> payParams=new HashMap<String, String>(); 
			payParams.put(EgamePay.PAY_PARAMS_KEY_TOOLS_ALIAS,Telecom_pay_id); 
			Pay(payParams); 
		}
	}
	 //Telecom callback
	private void Pay(HashMap<String, String> payParams)
	{ 
		EgamePay.pay(instance, payParams,new EgamePayListener() 
		{ 
			@Override 
			public void paySuccess(Map<String, String> params) 
			{ 
				Toast.makeText(MainActivity.this, "支付成功", 1000).show();
				UnityPlayer.UnitySendMessage("Main Camera", "Success",save_message);
			} 
			@Override 
			public void payFailed(Map<String, String> params, int errorInt) 
			{ 
				Toast.makeText(MainActivity.this, "支付失败:"+errorInt,1000).show();
			}
			@Override
			public void payCancel(Map<String, String> params)
			{ 
				Toast.makeText(MainActivity.this, "支付取消", 1000).show();
			}
		});
	} 
	//Unicon callback
	 private class paylistener implements UnipayPayResultListener
	 {
			
		 @Override
		 public void PayResult(String arg0, int arg1, int arg2, String err) 
		 {
			 switch (arg1) 
			 {
				 case 1://success
				 Toast.makeText(MainActivity.this, "支付成功", 1000).show();
				 UnityPlayer.UnitySendMessage("Main Camera", "Success",save_message);
				 break;
				
				 case 2://fail
				 Toast.makeText(MainActivity.this, "支付失败:"+err+"arg0="+arg0+"arg1="+arg1+"arg2="+arg2,1000).show();
				 Log.e("check","支付失败:"+err+"arg0="+arg0+"arg1="+arg1+"arg2="+arg2);
				 break;
				
				 case 3://cancel
				 Toast.makeText(MainActivity.this, "支付取消", 1000).show();
				 break;
				
				 default:
				 break;
			 }
		 }
	 }
	public void ExitGame() {
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				exitGame();
			}
		});
	}


	private void exitGame() {
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage("确认退出吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("", null);
		builder.setPositiveButton("确认", new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		builder.setNegativeButton("取消", new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
	}

}
