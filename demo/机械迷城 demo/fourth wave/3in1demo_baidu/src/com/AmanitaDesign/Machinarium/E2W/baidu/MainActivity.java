package com.AmanitaDesign.Machinarium.E2W.baidu;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.AmanitaDesign.Machinarium.E2W.baidu.R;
import com.duoku.platform.single.DKPlatform;
import com.duoku.platform.single.DKPlatformSettings;
import com.duoku.platform.single.DkErrorCode;
import com.duoku.platform.single.DkProtocolKeys;
import com.duoku.platform.single.callback.IDKSDKCallBack;
import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.Utils.UnipayPayResultListener;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
//import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import cn.cmgame.billing.api.BillingResult;
import cn.cmgame.billing.api.GameInterface;
import cn.egame.terminal.paysdk.EgamePay;
import cn.egame.terminal.paysdk.EgamePayListener;
import cn.egame.terminal.sdk.log.EgameAgent;

public class MainActivity extends Activity {
	private Context context;
	public static MainActivity instance;   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance=this;
		context =this;
		
		//baidu show
				IDKSDKCallBack initcompletelistener = new IDKSDKCallBack() {
					@Override
					public void onResponse(String paramString) {
						Log.d("GameMainActivity", paramString);
						try {
							JSONObject jsonObject = new JSONObject(paramString);
							int mFunctionCode = jsonObject
									.getInt(DkProtocolKeys.FUNCTION_CODE);
							if (mFunctionCode == DkErrorCode.BDG_CROSSRECOMMEND_INIT_FINSIH) {
								Log.e("Unity", "baidu sdk init ok");
								DKPlatform.getInstance().bdgameInit(instance,
										new IDKSDKCallBack() {
											@Override
											public void onResponse(String arg0) {
												// TODO Auto-generated method stub
												Log.e("Unity", "pinxuan sdk init ok");
											}
										});
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				};
				DKPlatform.getInstance().init(this, true,DKPlatformSettings.SdkMode.SDK_BASIC, null, null,initcompletelistener);
		
		//init mobile
		if(GameSdkApplication.netcheck==1||GameSdkApplication.netcheck==0)
			GameInterface.initializeApp(this);
		
		//init telecom
		if(GameSdkApplication.netcheck==3)
			EgamePay.init(this);
		
		
		
		setContentView(R.layout.activity_main);	
        Button btn = (Button)findViewById(R.id.button1);
        
        
      //button buy
        btn.setOnClickListener(new android.view.View.OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				
				
				//************************paying***************************
				
				//if it is mobile or can't read the SIM card
				if(GameSdkApplication.netcheck==1||GameSdkApplication.netcheck==0)
				{
					GameInterface.doBilling(context, true, true, "001", null, new GameInterface.IPayCallback() {
						
						@Override
						public void onResult(int resultCode, String billingIndex, Object obj) {
							// TODO Auto-generated method stub
							String result = "";
							final String index = billingIndex;
							switch(resultCode)
							{
								case BillingResult.SUCCESS:
									result = "支付成功";
									break;
								case BillingResult.FAILED:
									result = "支付失败";
									break;
								default:
									result = "支付取消";
									break;
							}
							Toast.makeText(instance, result, Toast.LENGTH_LONG).show();
						}
					} );
				}
				
				//if it is Unicon
				if(GameSdkApplication.netcheck==2)
				{
					Utils.getInstances().pay(MainActivity.this,"001",new paylistener());
				}
				
				//if it is telecom
				if(GameSdkApplication.netcheck==3)
				{
					
					HashMap<String, String> payParams=new HashMap<String, String>(); 
					payParams.put(EgamePay.PAY_PARAMS_KEY_TOOLS_ALIAS, "TOOL1"); 
					Pay(payParams); 
				}
			}
        });
        
        
  	  //****************************************exit     
        Button exit = (Button)findViewById(R.id.button2);
        exit.setOnClickListener(new android.view.View.OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				ExitGame();
			}
        });
	}
	 //Telecom callback
	private void Pay(HashMap<String, String> payParams)
	{ 
		EgamePay.pay(instance, payParams,new EgamePayListener() 
		{ 
			@Override 
			public void paySuccess(Map<String, String> params) 
			{ 
				Toast.makeText(MainActivity.this, "支付成功", 1000).show();
			} 
			@Override 
			public void payFailed(Map<String, String> params, int errorInt) 
			{ 
				Toast.makeText(MainActivity.this, "支付失败:"+errorInt,1000).show();
			}
			@Override
			public void payCancel(Map<String, String> params)
			{ 
				Toast.makeText(MainActivity.this, "支付取消", 1000).show();
			}
		});
	} 
	//Unicon callback
	 private class paylistener implements UnipayPayResultListener
	 {
			
		 @Override
		 public void PayResult(String arg0, int arg1, int arg2, String err) 
		 {
			 switch (arg1) 
			 {
				 case 1://success
				 Toast.makeText(MainActivity.this, "支付成功", 1000).show();
				 break;
				
				 case 2://fail
				 Toast.makeText(MainActivity.this, "支付失败:"+err+"arg0="+arg0+"arg1="+arg1+"arg2="+arg2,1000).show();
				 Log.e("check","支付失败:"+err+"arg0="+arg0+"arg1="+arg1+"arg2="+arg2);
				 break;
				
				 case 3://cancel
				 Toast.makeText(MainActivity.this, "支付取消", 1000).show();
				 break;
				
				 default:
				 break;
			 }
		 }
	 }
	
	
	@Override
	protected void onPause() {
		Log.e("Max", "MainActivity onPause");
		// TODO Auto-generated method stub
		if(GameSdkApplication.netcheck==2)
			Utils.getInstances().onPause(context);
		if(GameSdkApplication.netcheck==3)
			EgameAgent.onResume(this);
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.e("Max", "MainActivity onResume");
		// TODO Auto-generated method stub
		if(GameSdkApplication.netcheck==2)
			Utils.getInstances().onResume(context);
		if(GameSdkApplication.netcheck==3)
			EgameAgent.onResume(this);
		super.onResume();

	}
public void ExitGame() {
	runOnUiThread(new Runnable() {
		@Override
		public void run() {
			exitGame();
		}
	});
}


private void exitGame() {
//*********************************baidu exit***************************
	DKPlatform.getInstance().bdgameExit(instance, new IDKSDKCallBack() {
		@Override
		public void onResponse(String paramString) {
			finish();
			android.os.Process.killProcess(android.os.Process.myPid());
		}
	});
//*********************************baidu exit***************************
}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
