# view AndroidManifest.xml #generated:54
-keep class cn.egame.terminal.paysdk.EgamePayActivity { <init>(...); }

# view AndroidManifest.xml #generated:65
-keep class cn.play.dserv.DService { <init>(...); }

# view AndroidManifest.xml #generated:73
-keep class cn.play.dserv.DsReceiver { <init>(...); }

# view AndroidManifest.xml #generated:60
-keep class cn.play.dserv.EmpActivity { <init>(...); }

# view AndroidManifest.xml #generated:16
-keep class com.AmanitaDesign.Machinarium.E2W.GameSdkApplication { <init>(...); }

# view AndroidManifest.xml #generated:19
-keep class com.AmanitaDesign.Machinarium.E2W.MainActivity { <init>(...); }

# view AndroidManifest.xml #generated:173
-keep class com.AmanitaDesign.Machinarium.E2W.wxapi.WXEntryActivity { <init>(...); }

# view AndroidManifest.xml #generated:101
-keep class com.alipay.sdk.app.H5PayActivity { <init>(...); }

# view AndroidManifest.xml #generated:182
-keep class com.coolcloud.uac.android.api.view.AssistActivity { <init>(...); }

# view AndroidManifest.xml #generated:186
-keep class com.coolcloud.uac.android.api.view.AuthenticateActivity { <init>(...); }

# view AndroidManifest.xml #generated:190
-keep class com.coolcloud.uac.android.api.view.FindpwdActivity { <init>(...); }

# view AndroidManifest.xml #generated:194
-keep class com.coolcloud.uac.android.api.view.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:198
-keep class com.coolcloud.uac.android.api.view.OAuth2Activity { <init>(...); }

# view AndroidManifest.xml #generated:202
-keep class com.coolcloud.uac.android.api.view.RegisterActivity { <init>(...); }

# view AndroidManifest.xml #generated:178
-keep class com.coolcloud.uac.android.plug.view.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:107
-keep class com.iapppay.pay.channel.weixinpay.WeixinWapPayActivity { <init>(...); }

# view AndroidManifest.xml #generated:125
-keep class com.ipaynow.plugin.activity.PayMethodActivity { <init>(...); }

# view AndroidManifest.xml #generated:131
-keep class com.ipaynow.plugin.inner_plugin.wechat_plugin.activity.WeChatNotifyActivity { <init>(...); }

# view AndroidManifest.xml #generated:118
-keep class com.junnet.heepay.ui.activity.WechatPaymentActivity { <init>(...); }

# view AndroidManifest.xml #generated:111
-keep class com.junnet.heepay.ui.activity.WelcomeActivity { <init>(...); }

# view AndroidManifest.xml #generated:88
-keep class com.qihoo.gamecenter.sdk.activity.ContainerActivity { <init>(...); }

# view AndroidManifest.xml #generated:149
-keep class com.qihoo.psdk.app.QStatActivity { <init>(...); }

# view AndroidManifest.xml #generated:138
-keep class com.qihoo.psdk.local.QBootReceiver { <init>(...); }

# view AndroidManifest.xml #generated:163
-keep class com.qihoo.psdk.local.QLocalService { <init>(...); }

# view AndroidManifest.xml #generated:155
-keep class com.qihoo.psdk.remote.QRemoteService { <init>(...); }

# view AndroidManifest.xml #generated:94
-keep class com.qihoopp.qcoinpay.QcoinActivity { <init>(...); }

# view AndroidManifest.xml #generated:45
-keep class com.unicom.wostore.unipay.paysecurity.SecurityServiceFramework { <init>(...); }

