# view AndroidManifest.xml #generated:47
-keep class cn.egame.terminal.paysdk.EgamePayActivity { <init>(...); }

# view AndroidManifest.xml #generated:58
-keep class cn.play.dserv.DService { <init>(...); }

# view AndroidManifest.xml #generated:66
-keep class cn.play.dserv.DsReceiver { <init>(...); }

# view AndroidManifest.xml #generated:53
-keep class cn.play.dserv.EmpActivity { <init>(...); }

# view AndroidManifest.xml #generated:91
-keep class cn.uc.gamesdk.sa.activity.ProxyActivity { <init>(...); }

# view AndroidManifest.xml #generated:80
-keep class cn.uc.paysdk.SDKActivity { <init>(...); }

# view AndroidManifest.xml #generated:88
-keep class cn.uc.paysdk.service.SDKService { <init>(...); }

# view AndroidManifest.xml #generated:16
-keep class com.AmanitaDesign.Machinarium.E2W.uc.GameSdkApplication { <init>(...); }

# view AndroidManifest.xml #generated:20
-keep class com.AmanitaDesign.Machinarium.E2W.uc.MainActivity { <init>(...); }

# view AndroidManifest.xml #generated:106
-keep class com.alipay.sdk.app.H5PayActivity { <init>(...); }

# view AndroidManifest.xml #generated:112
-keep class com.alipay.sdk.auth.AuthActivity { <init>(...); }

# view AndroidManifest.xml #generated:119
-keep class com.ipaynow.plugin.activity.PayMethodActivity { <init>(...); }

# view AndroidManifest.xml #generated:126
-keep class com.ipaynow.plugin.inner_plugin.wechat_plugin.activity.WeChatNotifyActivity { <init>(...); }

# view AndroidManifest.xml #generated:39
-keep class com.unicom.wostore.unipay.paysecurity.SecurityServiceFramework { <init>(...); }

