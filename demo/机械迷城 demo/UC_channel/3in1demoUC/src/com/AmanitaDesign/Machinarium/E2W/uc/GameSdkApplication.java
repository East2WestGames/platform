package com.AmanitaDesign.Machinarium.E2W.uc;
import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.Utils.UnipayPayResultListener;

import android.app.Application;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;



public class GameSdkApplication extends Application {
	public static GameSdkApplication instance;    
	private Context context;
	public static int netcheck;
	@Override
	public void onCreate() {
		super.onCreate();
		instance=this;
		context =this;	
		Log.e("Max", "GameSdkApplication");
		
		//check SIM card
		TelephonyManager telManager = (TelephonyManager) instance.getSystemService(Context.TELEPHONY_SERVICE);
		String imsi = telManager.getSubscriberId();
		if (imsi != null) 
		{
			if (imsi.startsWith("46000") || imsi.startsWith("46002"))
			{
				
				//it is mobile SIM card so add megjb library,save code 1
				System.loadLibrary("megjb");
				netcheck=1;
				Log.e("Max","mobile_init");
			} 
			else if (imsi.startsWith("46001")) 
			{
				//it is Unicon SIM card, init it, save code 2
				Utils.getInstances().initSDK(this, new UnipayPayResultListener() {
					
					public void PayResult(String arg0, int arg1, int arg2, String arg3) {
						
					}
				});
				netcheck=2;
				Log.e("Max","Unicon_init");

			} 
			else if (imsi.startsWith("46003") || imsi.startsWith("20404")) 
			{
				//it is Telecom SIM card, just save code 3
				netcheck=3;
				Log.e("Max","Telecom_init");
			}

		}
		else
		{
			//if can't read it, save code 0,but load mobile library
			System.loadLibrary("megjb");
			netcheck=0;
			Toast.makeText(this, "未能识别的手机卡", Toast.LENGTH_LONG).show();
		}
		
		//show which code
		Log.e("Max","netchek="+netcheck);

	}
	
}
