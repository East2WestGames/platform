package com.AmanitaDesign.Machinarium.E2W.uc;

import java.util.HashMap;
import java.util.Map;

import com.AmanitaDesign.Machinarium.E2W.uc.R;
import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.Utils.UnipayPayResultListener;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
//import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import cn.cmgame.billing.api.BillingResult;
import cn.cmgame.billing.api.GameInterface;
import cn.egame.terminal.paysdk.EgamePay;
import cn.egame.terminal.paysdk.EgamePayListener;
import cn.egame.terminal.sdk.log.EgameAgent;
import cn.uc.gamesdk.sa.UCGameSdk;
import cn.uc.gamesdk.sa.iface.open.SDKConst;
import cn.uc.gamesdk.sa.iface.open.UCGameSDKStatusCode;
import cn.uc.paysdk.SDKCore;
import cn.uc.paysdk.face.commons.Response;
import cn.uc.paysdk.face.commons.SDKCallbackListener;
import cn.uc.paysdk.face.commons.SDKError;
import cn.uc.paysdk.face.commons.SDKProtocolKeys;

public class MainActivity extends Activity {
	private Context context;
	public static MainActivity instance;
	public static String pid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance=this;
		context =this;
		
		
		//UC code start
		UCGameSdk.defaultSdk().setCallback(SDKConst.PAY_INIT_LISTENER,
				new SDKCallbackListener() {
					@SuppressWarnings("unused")
					@Override
					public void onSuccessful(int statusCode, Response response) {

						if (response.getType() == Response.LISTENER_TYPE_INIT) 
						{
							Toast.makeText(context, "支付初始化成功!",
									Toast.LENGTH_LONG).show();
						} else if (response.getType() == Response.LISTENER_TYPE_PAY) 
																						
						{

							response.setMessage(Response.OPERATE_SUCCESS_MSG);

						}
					}

					@Override
					public void onErrorResponse(SDKError error) {
						
						String msg = error.getMessage();

						if (TextUtils.isEmpty(msg)) {
							msg = "SDK occur error!";
						}
						Toast.makeText(instance, msg, Toast.LENGTH_LONG).show();
					}
				});

		UCGameSdk.defaultSdk().setCallback(SDKConst.SDK_INIT_LISTENER,
				new cn.uc.gamesdk.sa.iface.open.UCCallbackListener<String>() {
					@Override
					public void callback(int statuscode, String msg) {
						switch (statuscode) {
						case UCGameSDKStatusCode.SUCCESS: {

						}
							break;
						default: {
						}
							break;
						}
					}
				});

		try {
			Bundle payInitData = new Bundle();
			UCGameSdk.defaultSdk().init(instance, payInitData);
		} catch (Exception e) {

		}
		//UC code end
		
		
		//init mobile
		if(GameSdkApplication.netcheck==1||GameSdkApplication.netcheck==0)
			GameInterface.initializeApp(this);
		
		//init telecom
		if(GameSdkApplication.netcheck==3)
			EgamePay.init(this);
		
		
		
		setContentView(R.layout.activity_main);	
        Button btn = (Button)findViewById(R.id.button1);
        
        
      //button buy
        btn.setOnClickListener(new android.view.View.OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{			
				//************************UC paying START ***************************
				if(GameSdkApplication.netcheck==1)
					pid="001";
				if(GameSdkApplication.netcheck==2)
					pid="001";
				if(GameSdkApplication.netcheck==3)
					pid="TOOL1";
				Intent payIntent = new Intent();
				payIntent.putExtra(SDKProtocolKeys.CP_ORDER_ID, ""+ System.currentTimeMillis());
				payIntent.putExtra(SDKProtocolKeys.APP_NAME,"机械迷城");
				payIntent.putExtra(SDKProtocolKeys.PRODUCT_NAME,"激活后续关卡");
				payIntent.putExtra(SDKProtocolKeys.AMOUNT,"30"); 
				payIntent.putExtra(SDKProtocolKeys.DEBUG_MODE, true);
				payIntent.putExtra(SDKProtocolKeys.PAY_CODE,pid); 
				Log.e("Unity","CP_ORDER_ID:" + System.currentTimeMillis()
								+ "\nAPP_NAME:"
								+ instance.getString(R.string.app_name)
								+ "\nPAY_CODE:" + pid);
				try 
				{
					SDKCore.pay(instance, payIntent,new SDKCallbackListener() 
					{
								@Override
								public void onSuccessful(int status,Response response) 
								{
									
									//pay successfully
									Toast.makeText(context, "支付成功!",Toast.LENGTH_LONG).show();
									
									if (response.getType() == Response.LISTENER_TYPE_INIT) 
									{
										Log.e("Unity",
												"----------------------------0");
									} else if (response.getType() == Response.LISTENER_TYPE_PAY) 
									{

										Log.e("Unity",
												"----------------------------1");
										response.setMessage(Response.OPERATE_SUCCESS_MSG);

										Log.e("Unity", response.getData());

									}
								}

								@Override
								public void onErrorResponse(SDKError error) 
								{
									String errorMsg = error.getMessage();
									if (TextUtils.isEmpty(errorMsg))
										errorMsg = "SDK occur error!";
								}
							});
				}
				catch (Exception ex) 
				{
					ex.printStackTrace();
				}
			    //************************UC paying end ***************************
			}
        });
        
        
  	  //****************************************exit     
        Button exit = (Button)findViewById(R.id.button2);
        exit.setOnClickListener(new android.view.View.OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				ExitGame();
			}
        });
	}
	 //Telecom callback
	private void Pay(HashMap<String, String> payParams)
	{ 
		EgamePay.pay(instance, payParams,new EgamePayListener() 
		{ 
			@Override 
			public void paySuccess(Map<String, String> params) 
			{ 
				Toast.makeText(MainActivity.this, "支付成功", 1000).show();
			} 
			@Override 
			public void payFailed(Map<String, String> params, int errorInt) 
			{ 
				Toast.makeText(MainActivity.this, "支付失败:"+errorInt,1000).show();
			}
			@Override
			public void payCancel(Map<String, String> params)
			{ 
				Toast.makeText(MainActivity.this, "支付取消", 1000).show();
			}
		});
	} 
	//Unicon callback
	 private class paylistener implements UnipayPayResultListener
	 {
			
		 @Override
		 public void PayResult(String arg0, int arg1, int arg2, String err) 
		 {
			 switch (arg1) 
			 {
				 case 1://success
				 Toast.makeText(MainActivity.this, "支付成功", 1000).show();
				 break;
				
				 case 2://fail
				 Toast.makeText(MainActivity.this, "支付失败:"+err+"arg0="+arg0+"arg1="+arg1+"arg2="+arg2,1000).show();
				 Log.e("check","支付失败:"+err+"arg0="+arg0+"arg1="+arg1+"arg2="+arg2);
				 break;
				
				 case 3://cancel
				 Toast.makeText(MainActivity.this, "支付取消", 1000).show();
				 break;
				
				 default:
				 break;
			 }
		 }
	 }
	
	
	@Override
	protected void onPause() {
		Log.e("Max", "MainActivity onPause");
		// TODO Auto-generated method stub
		if(GameSdkApplication.netcheck==2)
			Utils.getInstances().onPause(context);
		if(GameSdkApplication.netcheck==3)
			EgameAgent.onResume(this);
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.e("Max", "MainActivity onResume");
		// TODO Auto-generated method stub
		if(GameSdkApplication.netcheck==2)
			Utils.getInstances().onResume(context);
		if(GameSdkApplication.netcheck==3)
			EgameAgent.onResume(this);
		super.onResume();

	}
public void ExitGame() {
	runOnUiThread(new Runnable() {
		@Override
		public void run() {
			exitGame();
		}
	});
}


private void exitGame() {
//*********************************exitGame***************************
	AlertDialog.Builder builder = new Builder(this);
	builder.setMessage("确认退出吗？");
	builder.setTitle("提示");
	builder.setNegativeButton("", null);
	builder.setPositiveButton("确认", new android.content.DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			finish();
			android.os.Process.killProcess(android.os.Process.myPid());
		}
	});
	builder.setNegativeButton("取消", new android.content.DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.dismiss();
		}
	});
	builder.create().show();
//*********************************exitGame***************************
}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
