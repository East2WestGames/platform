package com.example.test;

import cn.cmgame.billing.api.BillingResult;
import cn.cmgame.billing.api.GameInterface;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Context context;
	public static MainActivity instance;   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//init
		GameInterface.initializeApp(this);
		//control sound
		if(GameInterface.isMusicEnabled())
		{
			
		}
		context=this;
		setContentView(R.layout.activity_main);	
        Button btn = (Button)findViewById(R.id.button1);
        
        
      //****************************************paying
        btn.setOnClickListener(new OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				//pay
				GameInterface.doBilling(context, true, true, "001", null, new GameInterface.IPayCallback() {
					
					@Override
					public void onResult(int resultCode, String billingIndex, Object obj) {
						// TODO Auto-generated method stub
						String result = "";
						final String index = billingIndex;
						switch(resultCode)
						{
							case BillingResult.SUCCESS:
								result = "支付成功";
								break;
							case BillingResult.FAILED:
								result = "支付失败";
								break;
							default:
								result = "支付取消";
								break;
						}
						//Toast.makeText(instance, result, Toast.LENGTH_LONG).show();
					}
				} );
			}
        });
        
        
  	  //****************************************exit     
        Button exit = (Button)findViewById(R.id.button2);
        exit.setOnClickListener(new OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				//pay
				GameInterface.exit(context);
			}
        });
	}
	
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
