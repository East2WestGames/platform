﻿using UnityEngine;
using System.Collections;

public class PayCode : MonoBehaviour {

	public static AndroidJavaObject _plugin;
	public static int diamonds=0;
	public static string sound="on";
	void Start () 
	{
			using (var pluginClass = new AndroidJavaClass( "pl.idreams.pottery.MainActivity" )) 
			{
				_plugin = pluginClass.CallStatic<AndroidJavaObject> ("getInstance");
			}
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	void Soundoff() 
	{
		sound = "off";
	}
	void OnGUI()
	{
		if(GUI.Button (new Rect (0, 0, 200, 100), "10 diamonds for 2.0￥"))
			_plugin.Call ("Buy", "10diamonds");

		if(GUI.Button (new Rect (0, 100, 200, 100),"60 diamonds for 10.0￥"))
			_plugin.Call ("Buy", "60diamonds");

		if(GUI.Button (new Rect (0, 200, 200, 100), "130 diamonds for 20.0￥"))
			_plugin.Call ("Buy", "130diamonds");

		if(GUI.Button (new Rect (0, 300, 200, 100), "210 diamonds for 30.0￥"))
			_plugin.Call ("Buy", "210diamonds");

		if(GUI.Button (new Rect (0, 400, 200, 100), "Exit Game"))
			_plugin.Call ("ExitGame");

		//show diamnds
		GUI.Button (new Rect (200, 000, 200, 200), "I have "+diamonds+" diamonds!");
		//show game sound is on or not
		GUI.Button (new Rect (400, 000, 200, 200), "game sound:"+sound);

	}
	void Success(string message)
	{
		if (message == "10diamonds") 
			diamonds+=10;

		if (message == "60diamonds") 
			diamonds+=60;

		if (message == "130diamonds") 
			diamonds+=130;

		if (message == "210diamonds") 
			diamonds+=210;
	}

}
