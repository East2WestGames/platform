package pl.idreams.pottery;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import cn.cmgame.billing.api.BillingResult;
import cn.cmgame.billing.api.GameInterface;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity {
	public static String Mobile_pay_id,save_message;
	public static MainActivity instance;    
	private Context context;

	public static int cost;
	public static String Log_Tag="Max";
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		instance=this;
		context =this;	
		// init mobile SDK.
		GameInterface.initializeApp(this);
		
		//get user selection of sound,if you have problem check "read me first.docx"
//		if(!GameInterface.isMusicEnabled())
//		{
//			UnityPlayer.UnitySendMessage("Main Camera", "Soundoff","");
//		}
	}

	@Override
	protected void onPause() {
		Log.e(Log_Tag, "MainActivity onPause");
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.e(Log_Tag, "MainActivity onResume");
		// TODO Auto-generated method stub
		super.onResume();

	}

	public static Object getInstance() 
	{
		Log.e(Log_Tag, "instance");
		return instance;
	}


	public void Buy(final String pid) {
		Log.e(Log_Tag, "Buy");
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				buy(pid);
			}
		});
	}


	public void buy(String message) {
		save_message=message;
		switch (message) {
		case "10diamonds":
			Mobile_pay_id="001";
			break;
		case "60diamonds":
			Mobile_pay_id="002";
			break;
		case "130diamonds":
			Mobile_pay_id="003";
			break;
		case "210diamonds":
			Mobile_pay_id="004";
			break;
		}
		//paying
		GameInterface.doBilling(context, true, true, Mobile_pay_id, null, new GameInterface.IPayCallback() {
			
			@Override
			public void onResult(int resultCode, String billingIndex, Object obj) {
				// TODO Auto-generated method stub
				String result = "";
				final String index = billingIndex;
				switch(resultCode)
				{
					case BillingResult.SUCCESS:
						result = "支付成功";
						UnityPlayer.UnitySendMessage("Main Camera", "Success",save_message);
						break;
					case BillingResult.FAILED:
						result = "支付失败";
						break;
					default:
						result = "支付取消";
						Log.e(Log_Tag, save_message);	
						break;
				}
				Toast.makeText(instance, result, Toast.LENGTH_LONG).show();
			}
		});
		

	}

	public void ExitGame() {
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				exitGame();
			}
		});
	}


	private void exitGame() {
		//mobile exit
		GameInterface.exit(context);
	}

}
