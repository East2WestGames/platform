package com.AmanitaDesign.Machinarium.E2W;

import java.util.HashMap;
import java.util.Map;

import cn.egame.terminal.paysdk.EgamePay;
import cn.egame.terminal.paysdk.EgamePayListener;
import cn.egame.terminal.sdk.log.EgameAgent;
import cn.play.dserv.CheckTool;
import cn.play.dserv.ExitCallBack;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Context context;
	Activity thisActivity;
	public static MainActivity instance;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try
		{
			EgamePay.init(this);
			Log.e("Max","ok");
		}
		catch(Exception e)
		{
			Log.e("Max",e.toString());
		}
		instance = this;
		context = this;
		thisActivity = this;

		setContentView(R.layout.activity_main);
		Button btn = (Button) findViewById(R.id.button1);

		// ****************************************paying
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// pay
				// 电信支付
						// TODO Auto-generated method stub
						try
						{
							Log.e("Max","111111111111111");
							HashMap<String, String> payParams = new HashMap<String, String>();
							payParams.put(EgamePay.PAY_PARAMS_KEY_TOOLS_ALIAS, "TOOL1");
							payParams.put(EgamePay.PAY_PARAMS_KEY_PRIORITY, "sms");
							Pay(payParams);
						}catch(Exception e)
						{
							Log.e("Max","22222222222222");
							Log.e("Max",e.toString());
						}

			}
		});

		// ****************************************exit
		Button exit = (Button) findViewById(R.id.button2);
		exit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// pay
				// 电信支付
				MainActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						CheckTool.exit(thisActivity, new ExitCallBack() {

							@Override
							public void exit() {

								finish();
							}

							@Override
							public void cancel() {


							}
						});
					}
				});
			}
		});
	}



	private void Pay(HashMap<String, String> payParams) {
		EgamePay.pay(instance, payParams, new EgamePayListener() {
			@Override
			public void paySuccess(Map<String, String> params) {
				Toast.makeText(MainActivity.this, "支付成功", 1000).show();
			}

			@Override
			public void payFailed(Map<String, String> params, int errorInt) {
				Toast.makeText(MainActivity.this, "支付失败：" + errorInt, 1000)
						.show();

			}

			@Override
			public void payCancel(Map<String, String> params) {
				Toast.makeText(MainActivity.this, "支付取消", 1000).show();

			}
		});
	}

	protected void onPause() {

		// TODO Auto-generated method stub
		super.onPause();
		EgameAgent.onPause(this);
	}

	@Override
	protected void onResume() {

		// TODO Auto-generated method stub
		super.onResume();
		EgameAgent.onResume(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
