package com.example.test;


import java.util.HashMap;
import java.util.Map;

import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.Utils.UnipayPayResultListener;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Context context;
	Activity thisActivity;
	public static MainActivity instance;   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		context=this;
		thisActivity=this;

		setContentView(R.layout.activity_main);	
        Button btn = (Button)findViewById(R.id.button1);
        
        
      //****************************************paying
        btn.setOnClickListener(new OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				Utils.getInstances().pay(MainActivity.this,"001",new paylistener());
			}
        });
        
        
  	  //****************************************exit     
        Button exit = (Button)findViewById(R.id.button2);
        exit.setOnClickListener(new OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{

			}
        });
	}
	 private class paylistener implements UnipayPayResultListener{
			
	 @Override
	 public void PayResult(String arg0, int arg1, int arg2, String err) {
	 switch (arg1) {
	 case 1://success
			
	 Toast.makeText(MainActivity.this, "支付成功", 1000).show();
	 break;
	
	 case 2://fail
	  //put fail code**********************
	  
	  //put fail code********************
	 Toast.makeText(MainActivity.this, "支付失败",1000).show();
	 Log.e("check","支付失败:"+err+"arg0="+arg0+"arg1="+arg1+"arg2="+arg2);
	 break;
	
	 case 3://cancel
	  //put cancel code********************  
	  //put cancel code********************
	 Toast.makeText(MainActivity.this, "支付取消", 1000).show();
	 break;
	
	 default:
	 break;
	
	 }
	
	 }
	 }
	protected void onPause() {

		// TODO Auto-generated method stub
		super.onPause();
	
	}

	@Override
	protected void onResume() {

		// TODO Auto-generated method stub
		super.onResume();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
