package pl.idreams.pottery;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.Utils.UnipayPayResultListener;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity {
	public static String Unicon_pay_id,save_message;
	public static MainActivity instance;    
	private Context context;

	public static int cost;
	public static String Log_Tag="Max";
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		instance=this;
		context =this;	
	}

	@Override
	protected void onPause() {
		Log.e(Log_Tag, "MainActivity onPause");
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	protected void onResume() {
		Log.e(Log_Tag, "MainActivity onResume");
		// TODO Auto-generated method stub
		super.onResume();

	}

	public static Object getInstance() 
	{
		Log.e(Log_Tag, "instance");
		return instance;
	}


	public void Buy(final String pid) {
		Log.e(Log_Tag, "Buy");
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				buy(pid);
			}
		});
	}


	public void buy(String message) {
		save_message=message;
		switch (message) {
		case "10diamonds":
			Unicon_pay_id="005";
			break;
		case "60diamonds":
			Unicon_pay_id="006";
			break;
		case "130diamonds":
			Unicon_pay_id="007";
			break;
		case "210diamonds":
			Unicon_pay_id="008";
			break;
		}
		//paying
		Utils.getInstances().pay(MainActivity.this,Unicon_pay_id,new paylistener());
	}
	
	//Unicon callback
	 private class paylistener implements UnipayPayResultListener
	 {
			
		 @Override
		 public void PayResult(String arg0, int arg1, int arg2, String err)
		 {
			 switch (arg1) 
			 {
				 case 1://success	
				 Toast.makeText(MainActivity.this, "支付成功", 1000).show();
				 UnityPlayer.UnitySendMessage("Main Camera", "Success",save_message);
				 break;
				 case 2://fail
				 Toast.makeText(MainActivity.this, "支付失败",1000).show();
				 Log.e("check","支付失败:"+err+"arg0="+arg0+"arg1="+arg1+"arg2="+arg2);
				 break;	
				 case 3://cancel
				 Toast.makeText(MainActivity.this, "支付取消", 1000).show();
				 break;	
				 default:
				 break;
		
			 }
		
		 }
	 }
	public void ExitGame() {
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				exitGame();
			}
		});
	}


	private void exitGame() {
		// exit
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage("确认退出吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("", null);
		builder.setPositiveButton("确认", new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		builder.setNegativeButton("取消", new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
	}
	
}
