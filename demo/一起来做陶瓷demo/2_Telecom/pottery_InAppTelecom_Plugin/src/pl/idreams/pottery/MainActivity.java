package pl.idreams.pottery;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import cn.cmgame.billing.api.BillingResult;
import cn.cmgame.billing.api.GameInterface;
import cn.egame.terminal.paysdk.EgamePay;
import cn.egame.terminal.paysdk.EgamePayListener;
import cn.egame.terminal.sdk.log.EgameAgent;
import cn.play.dserv.CheckTool;
import cn.play.dserv.ExitCallBack;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity {
	public static String Telecom_pay_id,save_message;
	public static MainActivity instance;    
	private Context context;

	public static int cost;
	public static String Log_Tag="Max";
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		instance=this;
		context =this;	
		// init telecom SDK.
		EgamePay.init(this);
	}

	@Override
	protected void onPause() {
		Log.e(Log_Tag, "MainActivity onPause");
		// TODO Auto-generated method stub
		super.onPause();
		EgameAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		Log.e(Log_Tag, "MainActivity onResume");
		// TODO Auto-generated method stub
		super.onResume();
		EgameAgent.onResume(this);
	}

	public static Object getInstance() 
	{
		Log.e(Log_Tag, "instance");
		return instance;
	}


	public void Buy(final String pid) {
		Log.e(Log_Tag, "Buy");
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				buy(pid);
			}
		});
	}


	public void buy(String message) {
		save_message=message;
		switch (message) {
		case "10diamonds":
			Telecom_pay_id="TOOL5";
			break;
		case "60diamonds":
			Telecom_pay_id="TOOL6";
			break;
		case "130diamonds":
			Telecom_pay_id="TOOL7";
			break;
		case "210diamonds":
			Telecom_pay_id="TOOL8";
			break;
		}
		//paying
				HashMap payParams=new HashMap();
				payParams.put(EgamePay.PAY_PARAMS_KEY_TOOLS_ALIAS,Telecom_pay_id);
				Pay(payParams);
	}
	  //Telecom callback
		private void Pay(HashMap<String, String> payParams)
		{ 
			EgamePay.pay(instance, payParams,new EgamePayListener() 
			{ 
				@Override 
				public void paySuccess(Map<String, String> params) 
				{ 
					Toast.makeText(MainActivity.this, "支付成功", 1000).show();
					Log.e(Log_Tag, "BUY_SUCCESS,save_message="+save_message);
					UnityPlayer.UnitySendMessage("Main Camera", "Success",save_message);
				} 
				@Override 
				public void payFailed(Map<String, String> params, int errorInt) 
				{ 
					Toast.makeText(MainActivity.this, "支付失败："+errorInt, 1000).show();
				}
				@Override
				public void payCancel(Map<String, String> params)
				{ 
					Toast.makeText(MainActivity.this, "支付取消", 1000).show();
				}
			});
		} 
	public void ExitGame() {
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				exitGame();
			}
		});
	}


	private void exitGame() {
		//mobile exit
		CheckTool.exit(instance, new ExitCallBack() { //Main.this为主Activity

			public void exit() {
				finish();
				//退出游戏操作
				//Main.this.finish();
			}
			public void cancel() {
				//取消退出，返回游戏
				
			}
		});	
	}

}
