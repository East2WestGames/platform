package com.example.test;


import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXTextObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;


public class MainActivity extends Activity {
	private Context context;
	Activity thisActivity;
	public static MainActivity instance;   
	public  IWXAPI api;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		context=this;
		thisActivity=this;

		setContentView(R.layout.activity_main);	
        Button btn = (Button)findViewById(R.id.button1);
        regToWx();
        
      //****************************************share
        btn.setOnClickListener(new OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				//pay
				WXTextObject textObj = new WXTextObject();
				textObj.text = "1";

				WXMediaMessage msg = new WXMediaMessage();
				msg.mediaObject = textObj;
				msg.description = "1";
				SendMessageToWX.Req req = new SendMessageToWX.Req();
				req.transaction = String.valueOf(System.currentTimeMillis());
				req.scene = SendMessageToWX.Req.WXSceneTimeline;
				req.message = msg;

				api.sendReq(req);
			}
        });
        
        
  	  //****************************************exit     
        Button exit = (Button)findViewById(R.id.button2);
        exit.setOnClickListener(new OnClickListener() 
        {			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				//pay

			}
        });
	}
	private void regToWx()
	{
		api = WXAPIFactory.createWXAPI(this, "wxdb14dba61d85df5d",true);
		api.registerApp("wxdb14dba61d85df5d");
	}
	private void exitGame() 
	{
		
		//****************************************电信游戏退出开始
			
				
	}

	protected void onPause() {

		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	protected void onResume() {

		// TODO Auto-generated method stub
		super.onResume();


	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
