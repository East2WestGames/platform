package com.mondotv.marcuslevel;
import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.Utils.UnipayPayResultListener;

import android.app.Application;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;



public class GameSdkApplication extends Application 
{
	public static GameSdkApplication instance;    
	private Context context;
	public static int netcheck;
	@Override
	public void onCreate() 
	{
		super.onCreate();
		instance=this;
		context =this;	
		Log.e("Max", "GameSdkApplication");
		
		TelephonyManager telManager = (TelephonyManager) instance.getSystemService(Context.TELEPHONY_SERVICE);
		String imsi = telManager.getSubscriberId();
		if (imsi != null) 
		{
			if (imsi.startsWith("46000") || imsi.startsWith("46002"))
			{
				//移动初始化
				netcheck=1;
				System.loadLibrary("megjb");
			} 
			else if (imsi.startsWith("46001")) 
			{
				//联通初始化
				Utils.getInstances().initSDK(this, new UnipayPayResultListener() {
					
					public void PayResult(String arg0, int arg1, int arg2, String arg3) {
						
					}
				});
				netcheck=2;
				Log.e("Max","LT_init");
			} 
			else if (imsi.startsWith("46003") || imsi.startsWith("20404")) 
			{
				netcheck=3;
				//电信初始化
				Log.e("Max","dx_init");
			}

		}
		else
		{
			//默认初始化，用于调试
			netcheck=0;
			Toast.makeText(this, "未能识别的手机卡", Toast.LENGTH_LONG).show();
		}
		Log.e("Max","netchek="+netcheck);
	}

}
