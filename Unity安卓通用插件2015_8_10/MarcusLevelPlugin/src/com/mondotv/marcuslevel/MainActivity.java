package com.mondotv.marcuslevel;

import java.util.HashMap;
import java.util.Map;

import mm.purchasesdk.Purchase;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import cn.cmgame.billing.api.BillingResult;
import cn.cmgame.billing.api.GameInterface;
import cn.egame.terminal.paysdk.EgamePay;
import cn.egame.terminal.paysdk.EgamePayListener;
import cn.egame.terminal.sdk.log.EgameAgent;

import com.mediav.ads.sdk.adcore.Mvad;
import com.unicom.dcLoader.Utils;
import com.unicom.dcLoader.Utils.UnipayPayResultListener;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity {
	public static String save_message,ydpid,dxpid,ltpid,description;
	public static MainActivity instance;    
	private Context context;
	public static Purchase purchase;
	public static int cost;
	public static String Log_Tag="Max";
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		instance=this;
		context =this;
		//移动基地初始化
		if(GameSdkApplication.netcheck==1)
			GameInterface.initializeApp(this);
		//电信初始化
		if(GameSdkApplication.netcheck==3)
			EgamePay.init(this);
	}

	@Override
	protected void onPause() {
		Log.e(Log_Tag, "MainActivity onPause");
		// TODO Auto-generated method stub
		if(GameSdkApplication.netcheck==2)
			Utils.getInstances().onPause(context);
		if(GameSdkApplication.netcheck==3)
			EgameAgent.onResume(this);
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.e(Log_Tag, "MainActivity onResume");
		// TODO Auto-generated method stub
		if(GameSdkApplication.netcheck==2)
			Utils.getInstances().onResume(context);
		if(GameSdkApplication.netcheck==3)
			EgameAgent.onResume(this);
		super.onResume();

	}
//*********************************获取实例***************************
	public static Object getInstance() 
	{

		Log.e(Log_Tag, "instance");
		return instance;
	}
//*********************************获取实例***************************
	
//*********************************显示log日志***************************
	public void Log(final String LogString) 
	{
		Log.e(Log_Tag,LogString);
	}
//*********************************显示log日志***************************
	
	public void Buy(final String pid) {
		Log.e(Log_Tag, "Buy");
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				buy(pid);
			}
		});
	}


	public void buy(String message) {
		save_message = message;
		switch (message) {
		case "Choco400":
			ydpid="001";
			dxpid="TOOL1";
			ltpid="001";
			description="400巧克力";
			cost=2;
			break;
		case "Choco1500":
			ydpid="002";
			dxpid="TOOL2";
			ltpid="002";
			description="1500巧克力";
			cost=6;
			break;
		case "Choco3500":
			ydpid="003";
			dxpid="TOOL3";
			ltpid="003";
			description="3500巧克力";
			cost=12;
			break;
		case "Choco6000":
			ydpid="004";
			dxpid="TOOL4";
			ltpid="004";
			description="6000巧克力";
			cost=18;
			break;
		case "Choco9600":
			ydpid="005";
			dxpid="TOOL5";
			ltpid="005";
			description="9600巧克力";
			cost=24;
			break;
		case "Choco15000":
			ydpid="006";
			dxpid="TOOL6";
			ltpid="006";
			description="15000巧克力";
			cost=30;
			break;
		case "Choco5000":
			ydpid="007";
			dxpid="TOOL7";
			ltpid="007";
			description="解锁全部场景";
			cost=30;
			break;
		}
		
		
		//移动基地支付
		if(GameSdkApplication.netcheck==1)
		{
		GameInterface.doBilling(context, true, true, ydpid, null, new GameInterface.IPayCallback() {
			
			@Override
			public void onResult(int resultCode, String billingIndex, Object obj) {
				// TODO Auto-generated method stub
				String result = "";
				final String index = billingIndex;
				switch(resultCode)
				{
					case BillingResult.SUCCESS:
						result = "支付成功";
						Log.e(Log_Tag, "BUY_SUCCESS,save_message="+save_message);
						if(save_message.startsWith("Choco5000"))
						{
							UnityPlayer.UnitySendMessage("UI Root", "onProductPurchaseSuccess",save_message);
							UnityPlayer.UnitySendMessage("Panel - UnlockLevel", "onProductPurchaseSuccess",save_message);
						}
						else
						{
							UnityPlayer.UnitySendMessage("CoinsShop", "onProductPurchaseSuccess",save_message);
							UnityPlayer.UnitySendMessage("Panel - BuyChoko", "onProductPurchaseSuccess",save_message);
							
						}
						break;
					case BillingResult.FAILED:
						result = "支付失败";
						break;
					default:
						result = "支付取消";
						break;
				}
				Toast.makeText(instance, result, Toast.LENGTH_LONG).show();
			}
		} );
		
		}
		if(GameSdkApplication.netcheck==2)
		{
			Utils.getInstances().pay(MainActivity.this,ltpid,new paylistener());
		}
		if(GameSdkApplication.netcheck==3)
		{
			//电信支付
			HashMap<String, String> payParams=new HashMap<String, String>(); 
			payParams.put(EgamePay.PAY_PARAMS_KEY_TOOLS_ALIAS, dxpid); 
			payParams.put(EgamePay.PAY_PARAMS_KEY_TOOLS_DESC,description); 
			Pay(payParams); 
		}
		if(GameSdkApplication.netcheck==0)
			Toast.makeText(instance, "未能识别的手机卡", Toast.LENGTH_LONG).show();
	}
    //电信支付回调
	private void Pay(HashMap<String, String> payParams)
	{ 
		EgamePay.pay(context, payParams,new EgamePayListener() 
		{ 
			@Override 
			public void paySuccess(Map<String, String> params) 
			{ 
				Toast.makeText(MainActivity.this, "支付成功", 1000).show();
				if(save_message.startsWith("Choco5000"))
				{
					UnityPlayer.UnitySendMessage("UI Root", "onProductPurchaseSuccess",save_message);
					UnityPlayer.UnitySendMessage("Panel - UnlockLevel", "onProductPurchaseSuccess",save_message);
				}
				else
				{
					UnityPlayer.UnitySendMessage("CoinsShop", "onProductPurchaseSuccess",save_message);
					UnityPlayer.UnitySendMessage("Panel - BuyChoko", "onProductPurchaseSuccess",save_message);
					
				}
			} 
			@Override 
			public void payFailed(Map<String, String> params, int errorInt) 
			{ 
				Toast.makeText(MainActivity.this, "支付失败:"+errorInt,1000).show();
				//UnityPlayer.UnitySendMessage("JavaCommand", "BuySuccess",MainActivity.savepid);
			}
			@Override
			public void payCancel(Map<String, String> params)
			{ 
				Toast.makeText(MainActivity.this, "支付取消", 1000).show();
				//UnityPlayer.UnitySendMessage("JavaCommand", "BuySuccess",MainActivity.savepid);
			}
		});
	} 
    //电信支付回调
	
	//联通支付回调
	 private class paylistener implements UnipayPayResultListener
	 {
			
		 @Override
		 public void PayResult(String arg0, int arg1, int arg2, String err) 
		 {
			 switch (arg1) 
			 {
				 case 1://success
				 //此处放置支付请求已提交的相关处理代码
				 Toast.makeText(MainActivity.this, "支付成功", 1000).show();
				 if(save_message.startsWith("Choco5000"))
					{
						UnityPlayer.UnitySendMessage("UI Root", "onProductPurchaseSuccess",save_message);
						UnityPlayer.UnitySendMessage("Panel - UnlockLevel", "onProductPurchaseSuccess",save_message);
					}
					else
					{
						UnityPlayer.UnitySendMessage("CoinsShop", "onProductPurchaseSuccess",save_message);
						UnityPlayer.UnitySendMessage("Panel - BuyChoko", "onProductPurchaseSuccess",save_message);
						
					}
				 break;
				
				 case 2://fail
				 //此处放置支付请求失败的相关处理代�?
				 Toast.makeText(MainActivity.this, "支付失败",1000).show();
				 break;
				
				 case 3://cancel
				 //此处放置支付请求被取消的相关处理代码
				 Toast.makeText(MainActivity.this, "支付取消", 1000).show();
				 break;
				
				 default:
				 break;
			 }
		 }
	 }
		//联通支付回调
	public void ExitGame() {
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				exitGame();
			}
		});
	}


	private void exitGame() {
//*********************************正常退出***************************
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage("确认退出吗？");
		builder.setTitle("提示");
		builder.setPositiveButton("确认", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		builder.setNegativeButton("取消", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
//*********************************正常退出***************************
	}

//*********************************有米广告***************************
	public void YMAd() 
	{
		Log.e(Log_Tag, "Ymad");
	}
//*********************************有米广告***************************

//*********************************360广告***************************
	public void _360Ad() {
		
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				show360ad();
			}
		});
		
		
		
	}
	protected void show360ad()
	{
		Log.e(Log_Tag, "360ad");
	}
//*********************************360广告***************************
}
