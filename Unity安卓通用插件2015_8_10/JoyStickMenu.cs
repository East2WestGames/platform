﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class JoyStickMenu : MonoBehaviour
{
	public GameObject[] btns;
	public int numPerLine = 0;
	public bool isHorizontal = true;
	public int defaultIndex = 0;
	public EventDelegate onBackButton;
	public bool scaleSelected = true;
	public bool isLoopIndex= false;
	Vector3[] originalScales;//=new Vector3[btns.Length];
	public static int selectedIndex = 0;
	float rightTimer = 0f;
	float leftTimer = 0f;
	float downTimer = 0f;
	float upTimer = 0f;
	public static bool isSupportJoystick = false;
	bool locktime=false,locktime1=false;
	int add=0,bdd=0;
	int realtimeSinceStartup = 0,realtimeSinceStartup1=0;
	void Start ()
	{
		if (PlayerPrefs.GetInt ("TVMode", 0) == 1) {
			isSupportJoystick = true;
			//Debug.Log (isSupportJoystick);
		}
		if (isSupportJoystick) {
			selectedIndex = defaultIndex;
			if (scaleSelected) {
				originalScales = new Vector3[btns.Length];
				//Debug.Log (originalScales);
				for (int i=0; i<btns.Length; i++) {
					originalScales [i] = btns [i].transform.localScale;
				}
			}
		}
		//Time.timeScale = 1;
	}

	void Update ()
	{
		if (isSupportJoystick) 
		{
			if (Input.GetAxis ("Horizontal") >= JoyStickConfig.menuSensitivity)
			{
				if(add==0)
					OnRightBtn ();
				else if(add - realtimeSinceStartup > 10) 
				{
					OnRightBtn ();
					add=0;
				}
				add++;
				if(!locktime)
				{
					realtimeSinceStartup =add;
					locktime=true;
				}

			} 
			else 
			{
				locktime=false;
				add=0;
			}

			if (Input.GetAxis ("Horizontal") <= -JoyStickConfig.menuSensitivity) 
			{
				if(bdd==0)
					OnLeftBtn ();
				else if (bdd - realtimeSinceStartup1 > 10) 
				{
					OnLeftBtn ();
					bdd=0;
				}
				bdd++;
				if(!locktime1)
				{
					realtimeSinceStartup1 =bdd;
					locktime1=true;
				}

			} 
			else 
			{
				locktime1=false;
				bdd=0;
			}

			if (Input.GetAxis ("Vertical") >= JoyStickConfig.menuSensitivity)
			{
				//Debug.Log (rightTimer);
				downTimer += Time.deltaTime;
				if (downTimer > JoyStickConfig.detectDelay)
				{
					downTimer = 0;
					OnDownBtn ();
				}
			}
			else 
			{
				downTimer = 0;
			}

			if (Input.GetAxis ("Vertical") <= -JoyStickConfig.menuSensitivity) 
			{
				upTimer += Time.deltaTime;
				if (upTimer > JoyStickConfig.detectDelay) 
				{
					upTimer = 0;
					OnUpBtn ();

				}
			}
			else 
			{
				upTimer = 0;
			}

			if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				OnLeftBtn ();
			}
		
			if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				OnRightBtn ();
			}

			if (Input.GetKeyDown (KeyCode.UpArrow))
			{
				OnUpBtn ();
			}

			if (Input.GetKeyDown (KeyCode.DownArrow)) 
			{
				OnDownBtn ();
			}

			if (scaleSelected) 
			{
				for (int i=0; i<btns.Length; i++)
				{
					//Debug.Log(originalScales);
					btns [i].transform.localScale = originalScales [i];
				}
				btns [selectedIndex].transform.localScale = originalScales [selectedIndex] * JoyStickConfig.menuBtnSelectedScale;
			}
			OnSelectedBtn (btns [selectedIndex]);
			OnSelectedBtn (selectedIndex);
			if (Input.GetKeyUp (KeyCode.Space) || Input.GetKeyUp (KeyCode.Return) || Input.GetKeyUp (KeyCode.JoystickButton0) || Input.GetKeyUp (KeyCode.Menu)||Input.GetKeyDown(JoyStickConfig.enter1)) 
			{
				btns [selectedIndex].SendMessage ("OnClick");
				btns [selectedIndex].SendMessage ("OnPress",true, SendMessageOptions.DontRequireReceiver);
				btns [selectedIndex].SendMessage ("OnPress", false,SendMessageOptions.DontRequireReceiver);
				//gameObject.SetActive (false);
				if (scaleSelected) 
				{
					btns [selectedIndex].transform.localScale = originalScales [selectedIndex];
				}
			}
		
			if (Input.GetKeyUp (KeyCode.Escape) || Input.GetKeyUp (KeyCode.JoystickButton1))
			{
				OnBackBtn ();
				if (onBackButton != null) 
				{
					onBackButton.Execute ();
				}
				//gameObject.SetActive (false);
				if (scaleSelected) 
				{
					btns [selectedIndex].transform.localScale = originalScales [selectedIndex];
				}
			}
		}
	}

	public virtual void OnUp ()
	{
			
	}
		
	public virtual void OnDown ()
	{
			
	}

	public virtual void OnSelectedBtn (GameObject selectedBtn)
	{
			
	}

	public virtual void OnSelectedBtn (int index)
	{
			
	}

	public virtual void OnBackBtn ()
	{
	
	}

	public void SetSelectedIndex (int i)
	{
		selectedIndex = i;
	}


	void MoveToNext ()
	{
		selectedIndex++;
		//		if (!btns [selectedIndex].activeInHierarchy) {
		//			selectedIndex++;
		//		}
		ClampIndex ();
	}
	
	void MoveToLast ()
	{
		selectedIndex--;
		//		if (!btns [selectedIndex].activeInHierarchy) {
		//			selectedIndex--;
		//		}
		ClampIndex ();
	}
	
	void MoveToNextLine ()
	{
		selectedIndex += numPerLine;
		ClampIndex ();
		//Debug.Log(selectedIndex);
	}
	
	void MoveToLastLine ()
	{
		selectedIndex -= numPerLine;
		ClampIndex ();
	}
	
	void ClampIndex(){
		if (isLoopIndex) {
			selectedIndex = selectedIndex<0? btns.Length+selectedIndex: (selectedIndex> btns.Length - 1? selectedIndex-btns.Length:Mathf.Clamp (selectedIndex, 0, btns.Length - 1));
		} else {
			selectedIndex = Mathf.Clamp (selectedIndex, 0, btns.Length - 1);
		}
	}












//	void MoveToNext ()
//	{
//		selectedIndex++;
//		selectedIndex = Mathf.Clamp (selectedIndex, 0, btns.Length - 1);
//	}
//
//	void MoveToLast ()
//	{
//		selectedIndex--;
//		selectedIndex = Mathf.Clamp (selectedIndex, 0, btns.Length - 1);
//	}
//
//	void MoveToNextLine ()
//	{
//		selectedIndex += numPerLine;
//		selectedIndex = Mathf.Clamp (selectedIndex, 0, btns.Length - 1);
//	}
//
//	void MoveToLastLine ()
//	{
//		selectedIndex -= numPerLine;
//		selectedIndex = Mathf.Clamp (selectedIndex, 0, btns.Length - 1);
//	}












	void OnRightBtn ()
	{
		if (isHorizontal) {
			MoveToNext ();
		} else {
			MoveToNextLine ();
		}
		//Debug.Log("OnRightBtn" +selectedIndex);
	}

	void OnLeftBtn ()
	{
		if (isHorizontal) {
			MoveToLast ();
		} else {
			MoveToLastLine ();
		}
		//Debug.Log("OnLeftBtn" +selectedIndex);
	}

	void OnUpBtn ()
	{
		if (isHorizontal) {
			MoveToLastLine ();
		} else {
			MoveToLast ();
		}
		OnUp ();
		//Debug.Log("OnUpBtn" +selectedIndex);
	}

	void OnDownBtn ()
	{
		if (isHorizontal) {
			MoveToNextLine ();
		} else {
			MoveToNext ();
		}
		OnDown ();
		//Debug.Log("OnDownBtn" +selectedIndex);
	}
}
