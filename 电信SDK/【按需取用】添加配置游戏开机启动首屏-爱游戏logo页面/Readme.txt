注意事项：
1.在Manifest.xml文件中按该文件夹下的activitys.txt配置新EgameLaunchActivity。
2.在asset目录下添加egame_sdk_egame_logo.png图片（注意不要重命名改图片）
3.【重要】之前接过老版本4.1.x,要使用此功能，必须使用v4.1.2版本libs下cn.egame.terminal.paysdk.jar替换之前的

显示逻辑：
在配置成功之后，游戏先启动这个EgameLaunchActivity，即爱游戏欢logo迎页面,显示大概3秒左右，会根据上述配置，查找到游戏的activity，启动进入游戏

自行嵌入logo及外放产品不要参考本文档