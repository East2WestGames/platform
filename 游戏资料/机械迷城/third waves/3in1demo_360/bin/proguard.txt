# view AndroidManifest.xml #generated:47
-keep class cn.egame.terminal.paysdk.EgamePayActivity { <init>(...); }

# view AndroidManifest.xml #generated:58
-keep class cn.play.dserv.DService { <init>(...); }

# view AndroidManifest.xml #generated:66
-keep class cn.play.dserv.DsReceiver { <init>(...); }

# view AndroidManifest.xml #generated:53
-keep class cn.play.dserv.EmpActivity { <init>(...); }

# view AndroidManifest.xml #generated:16
-keep class com.AmanitaDesign.Machinarium.E2W.GameSdkApplication { <init>(...); }

# view AndroidManifest.xml #generated:20
-keep class com.AmanitaDesign.Machinarium.E2W.MainActivity { <init>(...); }

# view AndroidManifest.xml #generated:151
-keep class com.coolcloud.uac.android.api.view.AssistActivity { <init>(...); }

# view AndroidManifest.xml #generated:155
-keep class com.coolcloud.uac.android.api.view.AuthenticateActivity { <init>(...); }

# view AndroidManifest.xml #generated:159
-keep class com.coolcloud.uac.android.api.view.FindpwdActivity { <init>(...); }

# view AndroidManifest.xml #generated:163
-keep class com.coolcloud.uac.android.api.view.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:167
-keep class com.coolcloud.uac.android.api.view.OAuth2Activity { <init>(...); }

# view AndroidManifest.xml #generated:171
-keep class com.coolcloud.uac.android.api.view.RegisterActivity { <init>(...); }

# view AndroidManifest.xml #generated:147
-keep class com.coolcloud.uac.android.plug.view.LoginActivity { <init>(...); }

# view AndroidManifest.xml #generated:135
-keep class com.mediocre.grannysmith.Main { <init>(...); }

# view AndroidManifest.xml #generated:80
-keep class com.qihoo.gamecenter.sdk.activity.ContainerActivity { <init>(...); }

# view AndroidManifest.xml #generated:112
-keep class com.qihoo.psdk.app.QStatActivity { <init>(...); }

# view AndroidManifest.xml #generated:101
-keep class com.qihoo.psdk.local.QBootReceiver { <init>(...); }

# view AndroidManifest.xml #generated:126
-keep class com.qihoo.psdk.local.QLocalService { <init>(...); }

# view AndroidManifest.xml #generated:118
-keep class com.qihoo.psdk.remote.QRemoteService { <init>(...); }

# view AndroidManifest.xml #generated:39
-keep class com.unicom.wostore.unipay.paysecurity.SecurityServiceFramework { <init>(...); }

